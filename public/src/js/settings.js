$(document).ready(function() {
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  var calendar =  $('#calendar').fullCalendar({
    locale: 'en',
    header: {
      center: 'prev title next',
      left: '',
      right: ''
    },
    buttonText:{
      prev: '<',
      next: '>'
    },
    editable: false,
    firstDay: 0,
    selectable: true,
    defaultView: 'month',
    titleFormat:'YYYY.MM',
    axisFormat: 'h:mm',
    allDaySlot: true,
    dayRender: function (date, cell) {
      var otherMonth = $(cell).hasClass('fc-other-month');
      var dataDate = $(cell).attr('data-date');
      var checkbox = '<div class="checkbox-wrap"><input type="checkbox" class="input-checkbox" value="'+dataDate+'" onchange="boxChange(this)" checked></div>';
      if(!otherMonth){
        cell.append(checkbox);
      }

      var fcSun = $(cell).hasClass('fc-sun');
      var fcSat = $(cell).hasClass('fc-sat');
      if(fcSat || fcSun){
        $(cell).find('.input-checkbox').prop('checked', false);
      } else {
        $(cell).find('.input-checkbox').prop('checked', true);
      }
    },
    dayClick: function (date, jsEvent, view) {
      var target = jsEvent.target;
      if(!$(target).hasClass('input-checkbox') && $(target).find('.input-checkbox').is(':checked')){
        $('#modal1').modal({
          show: true
        });
      } else if(!$(target).hasClass('input-checkbox') && $(target).find('.input-checkbox').is(':checked')===false) {
        alert('예약 가능 날짜로 변경해주세요.');
      }
    }
  });
});

function boxChange(box) {
  var isConfirm = '';
  if(box.checked){
    isConfirm = confirm("예약가능 날짜로 변경하시겠습니까?");
    if (isConfirm) {
      box.checked = true;
      alert('예약 가능 날짜로 변경되었습니다. 날짜를 선택하여 예약 가능 시간을 확인해주세요.');
      return true;
    } else {
      box.checked = false;
    }
    return false;
  } else {
    isConfirm = confirm("체크해제 시 예약이 불가능합니다. 적용하시겠습니까?");
    if (isConfirm) {
      box.checked = false;
      alert('선택한 날짜의 예약이 안 되도록 처리되었습니다.');
      return true;
    } else {
      box.checked = true;
    }
  }
}