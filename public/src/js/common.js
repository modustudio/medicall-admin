var phoneReg = [
  {'reg':/(.*)/, 'rep':"$1"},										//0
  {'reg':/(.*)/, 'rep':"$1"},										//1
  {'reg':/(.*)/, 'rep':"$1"},										//2
  {'reg':/(.*)/, 'rep':"$1"},										//3
  {'reg':/([0-9]{3})([0-9]*)/, 'rep':"$1-$2"},					//4
  {'reg':/([0-9]{3})([0-9]*)/, 'rep':"$1-$2"},					//5
  {'reg':/([0-9]{3})([0-9]*)/, 'rep':"$1-$2"},					//6
  {'reg':/([0-9]{3})([0-9]*)/, 'rep':"$1-$2"},					//7
  {'reg':/([0-9]{4})([0-9]{4})/, 'rep':"$1-$2"},					//8
  {'reg':/([0-9]{3})([0-9]{3})([0-9]*)/, 'rep':"$1-$2-$3", 'seoul':{'reg':/([0-9]{2})([0-9]{3})([0-9]*)/, 'rep':"$1-$2-$3"} },		//9
  {'reg':/([0-9]{3})([0-9]{3})([0-9]*)/, 'rep':"$1-$2-$3", 'seoul':{'reg':/([0-9]{2})([0-9]{4})([0-9]*)/, 'rep':"$1-$2-$3"}},		//10
  {'reg':/([0-9]{3})([0-9]{4})([0-9]*)/, 'rep':"$1-$2-$3"}		//11
];

var phoneCheck = function(obj){
  var str = obj.value.replace(/[^0-9]/g, '');
  var len = str.length;
  
  if( phoneReg[len].seoul && str.substr(0, 2) == '02'){
    str = str.replace(phoneReg[len].seoul.reg, phoneReg[len].seoul.rep);
  }else{
    str = str.replace(phoneReg[len].reg, phoneReg[len].rep);
  }
  obj.value = str;
}

$(document).ready(function () {
  var dropifyOtp = {
    messages: {
      default: '파일을 여기 끌어다 놓거나 클릭하십시오.',
      replace: '파일을 바꾸려면 드래그 앤 드롭하거나 클릭하십시오.',
      remove: '삭제',
      error: '죄송합니다.'
    },
    error: {
      'fileSize': '파일 크기가 너무 큽니다. (최대 {{ value }})',
      'minWidth': '이미지가 너무 작습니다. (최고 {{ value }}px)',
      'maxWidth': '이미지 너비가 너무 큽니다. (최대 {{ value }}px)',
      'minHeight': '이미지 높이가 너무 작습니다. (최소 {{ value }}px)',
      'maxHeight': '이미지 높이가 너무 큽니다. (최대 {{ value }}px )',
      'imageFormat': '이미지 형식이 허용되지 않습니다. ({{ value }}만 허용)',
      'fileExtension': '허용되지 않는 파일형식입니다. ({{ value }}만 허용)'
    }
  }

  $('.dropify').dropify(dropifyOtp);
  $('body').on('click', '.btn-duplicator', clone_model);
  $('body').on('click', '.btn-remove', remove);

  //Functions
  function clone_model() {
    var b = $(this).parent('.input-file-box'),
      c = $('.model').clone(true, true);

    c.removeClass('model');
    c.find('input').addClass('dropify');

    $(b).after(c);
    $('.dropify').dropify(dropifyOtp);
  }

  function remove() {
    $(this).closest('.input-file-box').remove();
  }
});